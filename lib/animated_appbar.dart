import 'package:flutter/material.dart';

class AnimatedAppBar extends StatefulWidget with PreferredSizeWidget {
  int width = 0;
  @override
  _AnimatedAppBarState createState() => _AnimatedAppBarState();

  @override
  Size get preferredSize => Size(50, 50);
}

class _AnimatedAppBarState extends State<AnimatedAppBar>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 2),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _animationController.forward();
    return SafeArea(
      child: AnimatedBuilder(
        animation: _animationController,
        builder: (context, child) => Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(color: Colors.green, boxShadow: [
            BoxShadow(
                color: Colors.black,
                spreadRadius: 3 * _animationController.value,
                blurRadius: 3 * _animationController.value)
          ]),
          height: 100 * _animationController.value,
          child: Text(
            'Custom Animated AppBar',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
          ),
        ),
      ),
    );
  }
}
